<?php

    header('Content-type: application/json');
    
    $link = new PDO('mysql:host=home.antoinealizarrouk.uni.me;port=443;dbname=falcondroid', 'root', 'FalconDroid');
    
    $result = "No Params Provided";

    if(isset($_GET['type']) && isset($_GET['id']) && isset($_GET['note']))
    {
        if($_GET['type'] == 'show')
        {
            $result = updateNoteShow($link, $_GET['id'], $_GET['note']);
        }
        else
        {
            $result = updateNoteEstablishment($link, $_GET['id'], $_GET['note']);
        }
    }
    echo json_encode(array('RESULT' => $result));


function updateNoteShow(PDO $link, $id, $note)
{
    $query_str = "UPDATE `show` SET Note = :note WHERE `show`.ID_Show = :id;";
    $query = $link->prepare($query_str);
    return $query->execute(array('id'   => $id,
                                 'note' => $note));
}

function updateNoteEstablishment(PDO $link, $id, $note)
{
    $query_str = "UPDATE establishment SET Note = :note WHERE establishment.ID_Shop = :id;";
    $query = $link->prepare($query_str);
    return $query->execute(array('id'   => $id,
                                 'note' => $note));
}

?>
