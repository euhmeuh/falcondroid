<?php

    header('Content-type: application/json');
    
    $link = new PDO('mysql:host=home.antoinealizarrouk.uni.me;port=443;dbname=falcondroid', 'root', 'FalconDroid');
     
    /*
     *************************************************************************** 
     *                          MAIN
     *************************************************************************** 
     */
     $result = array('SHOWS'          => getShows($link),
                     'establishments' => getEstablishments($link));
    
    
     echo json_encode($result);
    
    /*
     ***************************************************************************
     *                          FUNCTIONS
     ***************************************************************************
     */
    function getEstablishments(PDO $link)
    {
        $query_str = "SELECT * FROM establishment";
        $establshment_result = array();
        $query = $link->prepare($query_str);
        $query->execute();
        $i = 0;
        while($row = $query->fetch(PDO::FETCH_OBJ))
        {
            $establshment_result[$i] = array('ID_Shop'     => $row->ID_Shop,
                                             'Name'        => $row->Name,
                                             'Note'        => $row->Note,
                                             'Information' => $row->Information,
                                             'Location'    => $row->Location,
                                             'Longitude'   => $row->Longitude,
                                             'Latitude'    => $row->Latitude,
                                             'Menus'       => getMenus($row->ID_Shop, $link),
                                             'Products'    => getProducts($row->ID_Shop, $link));
            $i++;
        }
        $establshment_result['ESTABLISHMENT_NUMBER'] = $i;
        
        return $establshment_result;
    }

    function getMenus($ID_Shop, PDO $link)
    {
        $query_str_menues = "SELECT * FROM menu WHERE ID_Restaurant = :id";
        $menu_result = array();
        $query = $link->prepare($query_str_menues);
        $query->execute(array('id' => $ID_Shop));
        $i = 0;
        while($row = $query->fetch(PDO::FETCH_OBJ))
        {
            $menu_result[$i] = array('ID_Menu' => $row->ID_Menu,
                                     'Price'   => $row->Price,
                                     'Content' => $row->Content);
            $i++;
        }
        $menu_result['MENU_NUMBER'] = $i;
        
        return $menu_result;
    }
    
    function getProducts($ID_Shop, PDO $link)
    {
        $query_str_products = "SELECT product.ID_Product, Name, Price FROM product INNER JOIN sell ON product.ID_Product = sell.ID_Product WHERE sell.ID_Shop = :id";
        $products_result = array();
        $query= $link->prepare($query_str_products);
        $query->execute(array('id' => $ID_Shop));
        $i = 0;
        while($row = $query->fetch(PDO::FETCH_ASSOC))
        {
            $products_result[$i] = $row;
            $i++;
        }
        
        $products_result['PRODUCT_NUMBER'] = $i;
        
        return $products_result;
    }


    function getShows(PDO $link)
    {
        $query_str = "SELECT * FROM `show`;";
        $show_result = array();
        $query = $link->prepare($query_str);
        $query->execute();
        $i = 0;
        while($row = $query->fetch(PDO::FETCH_OBJ))
        {
            $show_result[$i] = array('ID_Show'          => $row->ID_Show,
                                     'Name'             => $row->Name,
                                     'Note'             => $row->Note,
                                     'Information'      => $row->Information,
                                     'Duration'         => $row->Duration,
                                     'Creation_Date'    => $row->Creation_Date,
                                     'Number_Actor'     => $row->Number_Actor,
                                     'Historical_Event' => $row->Historical_Event,
                                     'Location'         => $row->Location,
                                     'Longitude'        => $row->Longitude,
                                     'Latitude'         => $row->Latitude,
                                     'Questions'        => getQuestions($row->ID_Show, $link),
                                     'Anecdotes'        => getAnecdotes($row->ID_Show, $link),
                                     'TimeTable'        => getTimeTable($row->ID_Show, $link));
            $i++;
        }
        $show_result['SHOW_NUMBER'] = $i;
        return $show_result;
    }
    
    
    function getTimeTable($ID_Show, PDO $link)
    {
        $query_str_timetable = "SELECT * FROM timetable WHERE ID_SHOW = :id";
        $tt_result = array();
        $i = 0;
        $query = $link->prepare($query_str_timetable);
        $query->execute(array('id' => $ID_Show));
        while($row = $query->fetch(PDO::FETCH_OBJ))
        {
            $tt_result[$i] = array('ID_TimeTable' => $row->ID_Timetable,
                               'Play_Date'    => $row->Play_Date,
                               'Play_Hour'    => $row->Play_Hour);
            
            $i++;
        }
        
        $tt_result['TIMETABLE_NUMBER'] = $i;
        
        return $tt_result;
    }
    
    function getAnecdotes($ID_Show, PDO $link)
    {
        $query_str_anecdote = "SELECT * FROM anecdote WHERE ID_SHOW = :id";
        $a_result = array();
        $i = 0;
        $query = $link->prepare($query_str_anecdote);
        $query->execute(array('id' => $ID_Show));
        while($row = $query->fetch(PDO::FETCH_OBJ))
        {
            $a_result[$i] = array('ID_Anecdote' => $row->ID_Anecdote,
                               'Information' => $row->Information);
            $i++;
        }
        $a_result['ANECDOTE_NUMBER'] = $i;
        
        return $a_result;
    }
    
    function getQuestions($ID_Show, PDO $link)
    {
        $q_result = array();
        $i = 0;
        $query_str_questions = "SELECT * FROM question WHERE ID_SHOW = :id";
        $query = $link->prepare($query_str_questions);
        $query->execute(array('id' => $ID_Show));
        while($row = $query->fetch(PDO::FETCH_OBJ))
        {
            $q_result[$i] = array('ID_Question' => $row->ID_Question,
                               'Text' => $row->Text);
            $i++;
        }
        $q_result['QUESTION_NUMBER'] = $i;
        
        return $q_result;
    }
?>
