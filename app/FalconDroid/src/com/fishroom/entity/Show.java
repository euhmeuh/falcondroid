package com.fishroom.entity;

import java.util.Calendar;

public class Show
{

    private int         actorNumber;
    private String[]    anecdote;
    private Coordinates coordinates;
    private Calendar    creationDate;
    private Calendar    duration;
    private String      historicalEvents;
    private int         id;
    private String      information;
    private String      location;
    private String      name;
    private int         note;
    private String[]    questions;
    private Calendar[]  timeTable;

    public Show(int actorNumber, String[] anecdote, Coordinates coordinates,
            Calendar creationDate, Calendar duration, String historicalEvents,
            int id, String information, String location, String name, int note,
            String[] questions, Calendar[] timeTable)
    {

        this.actorNumber = actorNumber;
        this.anecdote = anecdote;
        this.coordinates = coordinates;
        this.creationDate = creationDate;
        this.duration = duration;
        this.historicalEvents = historicalEvents;
        this.id = id;
        this.information = information;
        this.location = location;
        this.name = name;
        this.note = note;
        this.questions = questions;
        this.timeTable = timeTable;
    }

    /**
     * @return the actorNumber
     */
    public int getActorNumber()
    {
        return actorNumber;
    }

    /**
     * @param actorNumber
     *            the actorNumber to set
     */
    public void setActorNumber(int actorNumber)
    {
        this.actorNumber = actorNumber;
    }

    /**
     * @return the anecdote
     */
    public String[] getAnecdote()
    {
        return anecdote;
    }

    /**
     * @param anecdote
     *            the anecdote to set
     */
    public void setAnecdote(String[] anecdote)
    {
        this.anecdote = anecdote;
    }

    /**
     * @return the coordinates
     */
    public Coordinates getCoordinates()
    {
        return coordinates;
    }

    /**
     * @param coordinates
     *            the coordinates to set
     */
    public void setCoordinates(Coordinates coordinates)
    {
        this.coordinates = coordinates;
    }

    /**
     * @return the creationDate
     */
    public Calendar getCreationDate()
    {
        return creationDate;
    }

    /**
     * @param creationDate
     *            the creationDate to set
     */
    public void setCreationDate(Calendar creationDate)
    {
        this.creationDate = creationDate;
    }

    /**
     * @return the duration
     */
    public Calendar getDuration()
    {
        return duration;
    }

    /**
     * @param duration
     *            the duration to set
     */
    public void setDuration(Calendar duration)
    {
        this.duration = duration;
    }

    /**
     * @return the historicalEvents
     */
    public String getHistoricalEvents()
    {
        return historicalEvents;
    }

    /**
     * @param historicalEvents
     *            the historicalEvents to set
     */
    public void setHistoricalEvents(String historicalEvents)
    {
        this.historicalEvents = historicalEvents;
    }

    /**
     * @return the id
     */
    public int getId()
    {
        return id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(int id)
    {
        this.id = id;
    }

    /**
     * @return the information
     */
    public String getInformation()
    {
        return information;
    }

    /**
     * @param information
     *            the information to set
     */
    public void setInformation(String information)
    {
        this.information = information;
    }

    /**
     * @return the location
     */
    public String getLocation()
    {
        return location;
    }

    /**
     * @param location
     *            the location to set
     */
    public void setLocation(String location)
    {
        this.location = location;
    }

    /**
     * @return the name
     */
    public String getName()
    {
        return name;
    }

    /**
     * @param name
     *            the name to set
     */
    public void setName(String name)
    {
        this.name = name;
    }

    /**
     * @return the note
     */
    public int getNote()
    {
        return note;
    }

    /**
     * @param note
     *            the note to set
     */
    public void setNote(int note)
    {
        this.note = note;
    }

    /**
     * @return the questions
     */
    public String[] getQuestions()
    {
        return questions;
    }

    /**
     * @param questions
     *            the questions to set
     */
    public void setQuestions(String[] questions)
    {
        this.questions = questions;
    }

    /**
     * @return the timeTable
     */
    public Calendar[] getTimeTable()
    {
        return timeTable;
    }

    /**
     * @param timeTable
     *            the timeTable to set
     */
    public void setTimeTable(Calendar[] timeTable)
    {
        this.timeTable = timeTable;
    }

    public String getOneLineReadableTimeTable()
    {
        StringBuilder builder = new StringBuilder();
        builder.append("Horaires : \n");
        for (Calendar c : this.timeTable)
        {
            builder.append("-");
            builder.append(cradeDateBuilder(c));
        }

        return builder.toString();
    }

    public String getMultiLineReadableTimeTable()
    {
        StringBuilder builder = new StringBuilder();
        for (Calendar c : this.timeTable)
        {
            builder.append("-");
            builder.append(cradeDateBuilder(c));
            builder.append("\n");
        }

        return builder.toString();
    }

    public static String cradeDateBuilder(Calendar cal)
    {
        String zero = "";
        int h = cal.get(Calendar.HOUR_OF_DAY);
        if (h == 0)
        {
            h = 12;
        }

        int m = cal.get(Calendar.MINUTE);
        if (m < 10)
        {
            zero = "0";
        } else
        {
            zero = "";
        }
        return h + "h" + zero + m;
    }
}
