package com.fishroom.entity;

/**
 * @author Julien
 * 
 */
public class Product {
	private int id;
	private String name;
	private float price;

	/**
	 * Create Product
	 * 
	 * @param id
	 * @param name
	 * @param price
	 */
	public Product(int id, String name, float price) {
		this.id = id;
		this.name = name;
		this.price = price;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * The id to set.
	 * 
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * The name to set.
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the price
	 */
	public float getPrice() {
		return price;
	}

	/**
	 * The price to set.
	 * 
	 * @param price
	 */
	public void setPrice(float price) {
		this.price = price;
	}
}
