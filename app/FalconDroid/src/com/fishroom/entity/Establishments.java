package com.fishroom.entity;

/**
 * @author Julien
 * 
 */
public class Establishments {

	private Coordinates coordinates;
	private int id;
	private String information;
	private String location;
	private Menu[] menu;
	private String name;
	private int note;
	private Product[] products;

	/**
	 * Create Establishments
	 * 
	 * @param coordinates
	 * @param id
	 * @param information
	 * @param location
	 * @param menu
	 * @param name
	 * @param note
	 * @param products
	 */
	public Establishments(Coordinates coordinates, int id, String information,
			String location, Menu[] menu, String name, int note,
			Product[] products) {

		this.coordinates = coordinates;
		this.id = id;
		this.information = information;
		this.location = location;
		this.menu = menu;
		this.name = name;
		this.note = note;
		this.products = products;
	}

	/**
	 * @return the coordinates
	 */
	public Coordinates getCoordinates() {
		return coordinates;
	}

	/**
	 * The coordinates to set.
	 * 
	 * @param coordinates
	 */
	public void setCoordinates(Coordinates coordinates) {
		this.coordinates = coordinates;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * The id to set.
	 * 
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the information
	 */
	public String getInformation() {
		return information;
	}

	/**
	 * The information to set.
	 * 
	 * @param information
	 */
	public void setInformation(String information) {
		this.information = information;
	}

	/**
	 * @return the location
	 */
	public String getLocation() {
		return location;
	}

	/**
	 * The location to set.
	 * 
	 * @param location
	 */
	public void setLocation(String location) {
		this.location = location;
	}

	/**
	 * @return the menu
	 */
	public Menu[] getMenu() {
		return menu;
	}

	/**
	 * The menu to set.
	 * 
	 * @param menu
	 */
	public void setMenu(Menu[] menu) {
		this.menu = menu;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * The name to set.
	 * 
	 * @param name
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the note
	 */
	public int getNote() {
		return note;
	}

	/**
	 * The note to set.
	 * 
	 * @param note
	 */
	public void setNote(int note) {
		this.note = note;
	}

	/**
	 * @return the product
	 */
	public Product[] getProducts() {
		return products;
	}

	/**
	 * The products to set.
	 * 
	 * @param products
	 */
	public void setProducts(Product[] products) {
		this.products = products;
	}
}