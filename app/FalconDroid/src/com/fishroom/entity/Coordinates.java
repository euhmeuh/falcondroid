package com.fishroom.entity;

public class Coordinates {

	private double latitude;
	private double longitude;
	
	
	public double getLatitude() {
		return latitude;
	}


	public void setLatitude(float latitude) {
		this.latitude = latitude;
	}


	public double getLongitude() {
		return longitude;
	}


	public void setLongitude(float longitude) {
		this.longitude = longitude;
	}


	public Coordinates(double d, double e) {
		this.latitude = d;
		this.longitude = e;
	}
	
}
