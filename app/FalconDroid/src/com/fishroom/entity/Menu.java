package com.fishroom.entity;

/**
 * @author Julien
 * 
 */
public class Menu {
	private int id;
	private String content;
	private float price;

	/**
	 * Create Menu
	 * 
	 * @param id
	 * @param content
	 * @param price
	 */
	public Menu(int id, String content, float price) {
		this.id = id;
		this.content = content;
		this.price = price;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * The id to set
	 * 
	 * @param id
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * The content to set
	 * 
	 * @param content
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the price
	 */
	public float getPrice() {
		return price;
	}

	/**
	 * The price to set
	 * 
	 * @param price
	 */
	public void setPrice(float price) {
		this.price = price;
	}
}
