package com.fishroom.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fishroom.falcondroid.R;

public class FragmentHome extends Fragment
{

    public static Fragment newInstance(Context context)
    {
        FragmentHome f = new FragmentHome();

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_home,
                null);
        return root;
    }

}