package com.fishroom.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.fishroom.adapters.ServiceAdapter;
import com.fishroom.entity.Establishments;
import com.fishroom.falcondroid.R;
import com.fishroom.logic.SingleEstablishments;

/**
 * @author Julien
 * 
 */
public class FragmentService extends Fragment {

	/**
	 * Create the Fragment for Service
	 * 
	 * @param context
	 * @return f
	 */
	public static Fragment newInstance(Context context) {
		FragmentService f = new FragmentService();

		return f;
	}

	/**
	 * Create the View.
	 * 
	 * @param inflater
	 * @param container
	 * @param savedInstanceState
	 * @return root
	 */
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		ViewGroup root = (ViewGroup) inflater.inflate(
				R.layout.fragment_service, null);
		RelativeLayout rel = (RelativeLayout) root
				.findViewById(R.id.service_root);
		rel.addView(getServiceList());
		return root;
	}

	/**
	 * 
	 * @return list
	 */
	private ListView getServiceList() {
		ListView list = new ListView(getActivity());
		ServiceAdapter adapter = new ServiceAdapter(getActivity(),
				SingleEstablishments.getInstance().getEstablishments());
		list.setAdapter(adapter);
		list.setOnItemClickListener(new ServiceListItemClickListener());

		return list;
	}

	/**
	 * @author Julien
	 * 
	 */
	private class ServiceListItemClickListener implements
	ListView.OnItemClickListener {
		public ServiceListItemClickListener() {
			/**
			 * TODO
			 */
		}

		/*
		 * (non-Javadoc)
		 * 
		 * @see
		 * android.widget.AdapterView.OnItemClickListener#onItemClick(android
		 * .widget.AdapterView, android.view.View, int, long)
		 */
		@Override
		public void onItemClick(AdapterView<?> parent, View view, int position,
				long id) {
			Fragment fragment = new FragmentDetailedServiceView();
			Bundle bundle = new Bundle();
			Establishments establishments = (Establishments) parent
					.getItemAtPosition(position);
			bundle.putInt(FragmentDetailedServiceView.ESTABLISHMENTS_ID_ARG,
					establishments.getId());
			fragment.setArguments(bundle);

			FragmentManager fm = getFragmentManager();
			fm.beginTransaction().replace(R.id.main_container, fragment)
			.commit();
		}
	}
}