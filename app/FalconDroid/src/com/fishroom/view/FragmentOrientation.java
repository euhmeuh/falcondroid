package com.fishroom.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.fishroom.entity.Establishments;
import com.fishroom.entity.Show;
import com.fishroom.falcondroid.R;
import com.fishroom.logic.ScheduleShow;
import com.fishroom.logic.SingleEstablishments;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class FragmentOrientation extends Fragment
{
	GoogleMap gMap;
    
    public static Fragment newInstance(Context context)
    {
        FragmentOrientation f = new FragmentOrientation();

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_orientation, null);
        
        /** Ouverture de la map en mode HYBRID (sattelite et maps en meme temps. */
        gMap = ((SupportMapFragment) getFragmentManager().findFragmentById(R.id.map)).getMap();
        gMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
        
        /** Zoom sur l'entr�e du Puy du Fou sans mettre de marker. */
        CameraPosition cameraPosition = new CameraPosition.Builder().target(new LatLng(46.8911741, -0.9346049)).zoom(15).build();
        gMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
        
        /** Rotation de la map */ 
        gMap.getUiSettings().setRotateGesturesEnabled(true);
        
        gMap.getUiSettings().setMyLocationButtonEnabled(true);
        gMap.setMyLocationEnabled(true);
        /** 
         * Spectacles (Couleur rouge): 
         * - Le Signe de Triomphe : 46.8853815	-0.9280818
         * - Les Vikings : 46.8869360	-0.9288114
         * - Le Bal des Oiseaux Fantomes :  46.8893777	-0.9251528
         * - Le secret de la Lance : 46.8910641	-0.9313112
         * - Mousquetaire de Richelieu : 46.8928384	-0.9329849
         * - Les Orgues de Feu : 46.8909908	-0.9283286
         * - Les Grandes Eaux : 46.8897150	-0.9287577
         * - La l�gende de Martin : 46.8897590	-0.9319549
         * - L'Odys�e du Puy du Fou : 46.8894217	-0.9307962
         * - Le Magicien M�nestrel : 46.8871633	-0.9276634
         * - Les Musiciens Traditionnels : 46.8882265	-0.9322124
         * - Les Automates Musiciens : 46.8913500	-0.9325557
         * 
         * Boutique (Couleur bleu):
         * - La Cit� M�di�val : 46.8886188	-0.9269606
         * - Le Bourg 1900 : 46.8911411	-0.9326469
         * 
         * Restaurant (Couleur verte):
         * - Restaurant Les Deux Couronnes : 46.8890038	-0.9328132
         * - Restaurant L'Orangerie : 46.8916690	-0.9337949 
         */
        
        /* ---------------------------------------------------------------------
    	 * Shows
    	 * ---------------------------------------------------------------------
    	 */
        
        /** R�cup�ration des coordonn�es des shows et affichage des markers sur google maps. */
        /* A DECOMMENTER UNE FOIS LA BDD MISE A JOUR
        Show[] shows = ScheduleShow.getInstance().getShows();
        for (int i = 0; i < shows.length; i++)
        {
            Show show = shows[i];
            gMap.addMarker(new MarkerOptions().position(new LatLng(show.getCoordinates().getLatitude(), show.getCoordinates().getLongitude()))
            		.title(show.getName())
            		.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
            		
        }*/
        
        gMap.addMarker(new MarkerOptions().position(new LatLng(46.8853815, -0.9280818)).title("Le Signe de Triomphe")
        		.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        gMap.addMarker(new MarkerOptions().position(new LatLng(46.8869360, -0.9288114)).title("Les Vikings")
        		.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        gMap.addMarker(new MarkerOptions().position(new LatLng(46.8893777, -0.9251528)).title("Le Bal des Oiseaux Fant�mes")
        		.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        gMap.addMarker(new MarkerOptions().position(new LatLng(46.8910641, -0.9313112)).title("Le secret de la Lance")
        		.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        gMap.addMarker(new MarkerOptions().position(new LatLng(46.8928384, -0.9329849)).title("Mousquetaire de Richelieu")
        		.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        gMap.addMarker(new MarkerOptions().position(new LatLng(46.8909908, -0.9283286)).title("Les Orgues de Feu")
        		.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        gMap.addMarker(new MarkerOptions().position(new LatLng(46.8897150, -0.9287577)).title("Les Grandes Eaux")
        		.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        gMap.addMarker(new MarkerOptions().position(new LatLng(46.8897590, -0.9319549)).title("La l�gende de Martin")
        		.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        gMap.addMarker(new MarkerOptions().position(new LatLng(46.8894217, -0.9307962)).title("L'Odys�e du Puy du Fou")
        		.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        gMap.addMarker(new MarkerOptions().position(new LatLng(46.8871633, -0.9276634)).title("Le Magicien M�nestrel")
        		.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        gMap.addMarker(new MarkerOptions().position(new LatLng(46.8882265, -0.9322124)).title("Les Musiciens Traditionnels")
        		.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        gMap.addMarker(new MarkerOptions().position(new LatLng(46.8913500, -0.9325557)).title("Les Automates Musiciens")
        		.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_RED)));
        
        /* ---------------------------------------------------------------------
    	 * Establisments
    	 * ---------------------------------------------------------------------
    	 */
        /** R�cup�ration des coordonn�es des establishments et affichage des markers sur google maps. 
         * Fait la diff�rence entre les restaurants et les boutiques pour la couleur des markers
         */
        /* A DECOMMENTER UNE FOIS LA BDD MISE A JOUR
        Establishments[] establishments = SingleEstablishments.getInstance().getEstablishments();
        for (int i = 0; i < establishments.length; i++)
        {
            Establishments establishment = establishments[i];
            
            if (establishment.getMenu().length > 0)
            {
            	gMap.addMarker(new MarkerOptions().position(new LatLng(establishment.getCoordinates().getLatitude(), establishment.getCoordinates().getLongitude()))
                		.title(establishment.getName())
                		.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
            }
            else if (establishment.getProducts().length > 0)
            {
            	gMap.addMarker(new MarkerOptions().position(new LatLng(establishment.getCoordinates().getLatitude(), establishment.getCoordinates().getLongitude()))
                		.title(establishment.getName())
                		.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
            }
        }*/
        
        gMap.addMarker(new MarkerOptions().position(new LatLng(46.8911411, -0.9326469)).title("Le Bourg 1900")
        		.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
        gMap.addMarker(new MarkerOptions().position(new LatLng(46.8886188, -0.9269606)).title("La Cit� M�di�vale")
        		.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_BLUE)));
        gMap.addMarker(new MarkerOptions().position(new LatLng(46.8890038, -0.9328132)).title("Les Deux Couronnes")
        		.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
        gMap.addMarker(new MarkerOptions().position(new LatLng(46.8916690, -0.9337949)).title("L'Orangerie")
        		.icon(BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN)));
        
        return root;
    }

    @Override
    public void onDestroyView() {
	    super.onDestroyView();
	    
	    try {
            Fragment fragment = (getFragmentManager().findFragmentById(R.id.map));  
            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.remove(fragment);
            ft.commit();
        } catch (Exception e) {
            e.printStackTrace();
        }
	}
    
    public boolean onMarkerClick(final Marker marker)
    {
    	
		return false;
    }
}
