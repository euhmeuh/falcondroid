package com.fishroom.view;

import java.util.List;

import com.fishroom.falcondroid.R;
import com.google.android.gms.plus.PlusShare;
import com.google.android.gms.plus.PlusShare.Builder;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;

public class FragmentShare extends Fragment {
	 
	private EditText text_fb;
	private EditText text_tw;
	private EditText text_gg;
	private Button share_fb;
	private Button share_tw;
	private Button share_gg;
	
	public static Fragment newInstance(Context context) {
    	FragmentShare f = new FragmentShare();
 
        return f;
    }
 
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState) {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_share, null);
        
        TabHost tabhost = (TabHost) root.findViewById(R.id.tabhost_share);
        tabhost.setup();
        setNewTab(getActivity(), tabhost, "tab1", R.string.txt_tab_fb,
                R.id.tab_fb);
        setNewTab(getActivity(), tabhost, "tab2", R.string.txt_tab_tw,
                R.id.tab_tw);
        setNewTab(getActivity(), tabhost, "tab3", R.string.txt_tab_gg,
                R.id.tab_gg);
        
        
        text_tw = (EditText) root.findViewById(R.id.txt_to_share_tw);
        text_gg = (EditText) root.findViewById(R.id.txt_to_share_gg);
        share_fb = (Button) root.findViewById(R.id.btn_share_fb);
        share_tw = (Button) root.findViewById(R.id.btn_share_tw);
        share_gg = (Button) root.findViewById(R.id.btn_share_gg);
        
        share_fb.setOnClickListener(
        		new OnClickListener() {
        			@Override
        			public void onClick(View v) {
        				
        				Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        				shareIntent.setType("text/plain");
        				//shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, text_fb.getText().toString());
        				PackageManager pm = v.getContext().getPackageManager();
        				List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
        				for (final ResolveInfo app : activityList) {
        					if ((app.activityInfo.name).contains("facebook")) {
        						final ActivityInfo activity = app.activityInfo;
        						final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
        						shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        						shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
        						shareIntent.setComponent(name);
        						v.getContext().startActivity(shareIntent);
        						break;
        					}
        				}
        				
        				/*final Intent intentShare = new Intent(Intent.ACTION_SEND);
		        		intentShare.setType("text/plain");
		        		intentShare.putExtra(Intent.EXTRA_TEXT, text.getText().toString());
		        		getActivity().startActivity(Intent.createChooser(intentShare,"Partager avec..."));*/
        			}
        		}
        		);
        
        share_tw.setOnClickListener(
        		new OnClickListener() {
        			@Override
        			public void onClick(View v) {
        				Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
        				shareIntent.setType("text/plain");
        				shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, text_tw.getText().toString());
        				PackageManager pm = v.getContext().getPackageManager();
        				List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
        				for (final ResolveInfo app : activityList) {
        					if ((app.activityInfo.name).contains("twitter")) {
        						//if ("com.twitter.android.PostActivity".equals(app.activityInfo.name)) {
        						final ActivityInfo activity = app.activityInfo;
        						final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
        						shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
        						shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
        						shareIntent.setComponent(name);
        						v.getContext().startActivity(shareIntent);
        						break;
        					}
        				}
        			}
        		}
        		);
        
        share_gg.setOnClickListener(
        		new OnClickListener() {
		        	@Override
		        	public void onClick(View v) {
		        		/*Intent shareIntent = new Intent(android.content.Intent.ACTION_SEND);
		        		shareIntent.setType("text/plain");
		        		shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, text_gg.getText().toString());
		        		PackageManager pm = v.getContext().getPackageManager();
		        		List<ResolveInfo> activityList = pm.queryIntentActivities(shareIntent, 0);
		        		for (final ResolveInfo app : activityList) {
		        			if ((app.activityInfo.name).contains("google")) {
		        		    //if ("com.twitter.android.PostActivity".equals(app.activityInfo.name)) {
		        		        final ActivityInfo activity = app.activityInfo;
		        		        final ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);
		        		        shareIntent.addCategory(Intent.CATEGORY_LAUNCHER);
		        		        shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED);
		        		        shareIntent.setComponent(name);
		        		        shareIntent.setC
		        		        v.getContext().startActivity(shareIntent);
		        		        break;
		        		   }
		        		}*/
		        		
		        		Intent shareIntent = new PlusShare.Builder(getActivity())
		                .setType("text/plain")
		                .setContentUrl(Uri.parse("https://developers.google.com/+/"))
		                .getIntent();
		        		
		        		shareIntent.putExtra(android.content.Intent.EXTRA_TEXT, text_gg.getText().toString());

		        		v.getContext().startActivity(shareIntent);
		        	}
	        	}
        	);
        
        return root;
    }
    
    private void setNewTab(Context context, TabHost tabHost, String tag,
            int title, int contentID)
    {
        TabSpec tabSpec = tabHost.newTabSpec(tag);
        tabSpec.setIndicator(getString(title));
        tabSpec.setContent(contentID);
        tabHost.addTab(tabSpec);
    }
 
}