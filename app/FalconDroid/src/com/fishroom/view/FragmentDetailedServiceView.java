package com.fishroom.view;

import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.fishroom.JSON.JSONParser;
import com.fishroom.entity.Establishments;
import com.fishroom.entity.Menu;
import com.fishroom.entity.Product;
import com.fishroom.falcondroid.R;
import com.fishroom.logic.SingleEstablishments;

/**
 * @author Julien
 * 
 */
public class FragmentDetailedServiceView extends Fragment
{
    public static final String ESTABLISHMENTS_ID_ARG = "establishmentsId";
    public static final String MENU_ID_ARG           = "menuId";
    public static final String PRODUCT_ID_ARG        = "productId";
    private Establishments     establishment;

    /**
	 * 
	 */
    public FragmentDetailedServiceView()
    {

    }

    /*
     * (non-Javadoc)
     * 
     * @see
     * android.support.v4.app.Fragment#onCreateView(android.view.LayoutInflater,
     * android.view.ViewGroup, android.os.Bundle)
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle saveInstanceState)
    {
        View rootView = inflater.inflate(
                R.layout.fragment_detailed_service_information, container,
                false);

        int i = getArguments().getInt(
                FragmentDetailedServiceView.ESTABLISHMENTS_ID_ARG);
        establishment = SingleEstablishments.getInstance().getEstablishments(i);

        /**
         * Récupération de l'imageview et affichage de l'image en fonction de
         * l'id de l'Establishment
         */
        ImageView imageview = (ImageView) rootView
                .findViewById(R.id.ImageService);
        int nEstablismentID = getResources().getIdentifier(
                "establishment" + establishment.getId(), "drawable",
                getActivity().getPackageName());
        imageview.setImageResource(nEstablismentID);

        /**
         * Récupération du textview ServiceName et affichage du nom en fonction
         * du nom de l'Establishment
         */
        TextView text = (TextView) rootView.findViewById(R.id.ServiceName);
        text.setText(establishment.getName());

        /**
         * Récupération du textview ServiceNameDetails et affichage des
         * informations du service en fonction du nom de l'Establishment
         */
        text = (TextView) rootView.findViewById(R.id.ServiceNameDetails);
        text.setText(establishment.getInformation());

        /**
         * Récupération du RatingBar ServiceNotesDetails et affichage de la note
         * du service en fonction du nom de l'Establisment
         */
        RatingBar rate = (RatingBar) rootView
                .findViewById(R.id.ServiceNotesDetails);
        rate.setRating(Float.parseFloat(String.valueOf(establishment.getNote())) / 100.0f);
        rate.setOnRatingBarChangeListener(new changeRatingListener());

        /**
         * Récupération du textview ServiceLocalisationDetails et affichage du
         * nom en fonction du nom de l'Establishment
         */
        text = (TextView) rootView
                .findViewById(R.id.ServiceLocalisationDetails);
        text.setText(establishment.getLocation());

        /**
         * On regarde si le champs Menu est vide sur le service en question,
         * s'il n'est pas vide, on remplit le ServiceMenuProduit avec toutes ces
         * informations. S'il est vide, on regarde si le champs Product est vide
         * sur le service en question, s'il n'est pas vide on remplit le
         * ServiceMenuProduit avec toutes ces informations.
         */
        if (establishment.getMenu().length > 0)
        {
            Menu[] menus = establishment.getMenu();
            Menu menu = menus[0];

            TextView textmenu = (TextView) rootView
                    .findViewById(R.id.ServiceMenuProduit);
            textmenu.setText("Menus :");

            text = (TextView) rootView
                    .findViewById(R.id.ServiceMenuProduitDetails);
            text.setText(String.valueOf(menu.getPrice()) + " € - "
                    + menu.getContent());

        } else if (establishment.getProducts().length > 0)
        {
            Product[] product = establishment.getProducts();
            String cheatproduct = "";

            TextView textmenu = (TextView) rootView
                    .findViewById(R.id.ServiceMenuProduit);
            textmenu.setText("Produits :");

            text = (TextView) rootView
                    .findViewById(R.id.ServiceMenuProduitDetails);

            for (int p = 0; p < product.length; p++)
            {
                Product products = product[p];
                cheatproduct = cheatproduct + products.getPrice() + " € - "
                        + products.getName() + "\n";
            }
            text.setText(cheatproduct);
        }

        return rootView;
    }

    private class changeRatingListener implements
            RatingBar.OnRatingBarChangeListener
    {

        /*
         * (non-Javadoc)
         * 
         * @see
         * android.widget.RatingBar.OnRatingBarChangeListener#onRatingChanged
         * (android.widget.RatingBar, float, boolean)
         */
        @Override
        public void onRatingChanged(RatingBar ratingBar, float rating,
                boolean fromUser)
        {
            if (fromUser)
            {
                Float newRating = ((Float.parseFloat(String
                        .valueOf(establishment.getNote())) * 100.0f) + (rating * 100.0f)) / 2;
                UpdateNotation update = new UpdateNotation();
                update.execute(newRating);
                ratingBar.setRating(newRating / 100.0f);
                ratingBar.setIsIndicator(true);
            }
        }
    }

    public class UpdateNotation extends AsyncTask<Float, Void, Void>
    {
        private final String url = "http://aryl.dnsalias.org/submit.php?type=establishment";

        @Override
        protected Void doInBackground(Float... params)
        {
            String builded = url + "&id=" + establishment.getId() + "&note="
                    + params[0];
            JSONParser parser = new JSONParser();
            JSONObject object = parser.getJSONFromUrl(builded);
            String result;
            try
            {
                result = object.getString("RESULT");
                if (result.equals("true"))
                {
                    Log.i(UpdateNotation.class.toString(), "Notation Saved");
                } else
                {
                    Log.w(UpdateNotation.class.toString(),
                            "Couldn't record notation");
                }
            } catch (JSONException e)
            {
                // TODO Auto-generated catch block
                Log.e(JSONObject.class.toString(), "Failed to parse JSON");
            }
            return null;
        }
    }
}
