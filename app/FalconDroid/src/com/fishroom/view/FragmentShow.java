package com.fishroom.view;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.RelativeLayout;

import com.fishroom.adapters.ShowAdapter;
import com.fishroom.entity.Show;
import com.fishroom.falcondroid.R;
import com.fishroom.logic.ScheduleShow;

public class FragmentShow extends Fragment
{

    public static Fragment newInstance(Context context)
    {
        FragmentShow s = new FragmentShow();

        return s;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_show,
                null);
        RelativeLayout rel = (RelativeLayout) root.findViewById(R.id.show_root);
        rel.addView(getShowList());
        return root;
    }

    private ListView getShowList()
    {
        ListView list = new ListView(getActivity());
        ShowAdapter adapter = new ShowAdapter(getActivity(), ScheduleShow
                .getInstance().getShows());
        list.setAdapter(adapter);
        list.setOnItemClickListener(new ShowListItemClickListener());
        return list;
    }

    private class ShowListItemClickListener implements
            ListView.OnItemClickListener
    {

        /**
         * 
         */
        public ShowListItemClickListener()
        {
            // TODO Auto-generated constructor stub
        }

        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position,
                long id)
        {
            Fragment fragment = new FragmentDetailedShowView();
            Bundle args = new Bundle();
            Show show = (Show) parent.getItemAtPosition(position);
            args.putInt(FragmentDetailedShowView.SHOW_ID_ARG, show.getId());
            fragment.setArguments(args);

            FragmentManager fm = getFragmentManager();

            fm.beginTransaction().replace(R.id.main_container, fragment)
                    .commit();
        }
    }

}
