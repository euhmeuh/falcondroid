package com.fishroom.view;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import org.apache.http.NameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import com.fishroom.JSON.JSONParser;
import com.fishroom.entity.Coordinates;
import com.fishroom.entity.Establishments;
import com.fishroom.entity.Menu;
import com.fishroom.entity.Product;
import com.fishroom.entity.Show;
import com.fishroom.falcondroid.R;
import com.fishroom.logic.ScheduleShow;
import com.fishroom.logic.SingleEstablishments;

import android.content.Intent;
import android.content.res.Configuration;
import android.net.ParseException;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;

public class MainActivity extends ActionBarActivity {

	private DrawerLayout mDrawerLayout;
    private ActionBarDrawerToggle mDrawerToggle;
    final String[] fragments = {
    		"com.fishroom.view.FragmentHome",
    		"com.fishroom.view.FragmentSchedule",
    		"com.fishroom.view.FragmentShow",
    		"com.fishroom.view.FragmentService",
    		"com.fishroom.view.FragmentShare",    		
    		"com.fishroom.view.FragmentQueue",    		
    		"com.fishroom.view.FragmentOrientation",
    		"com.fishroom.view.Picture"
    };
    //private EntityManager EntityCreate;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        //EntityCreate = new EntityManager();
        setContentView(R.layout.activity_main);
        FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
    	tx.replace(R.id.main_container, Fragment.instantiate(MainActivity.this, fragments[0]));
    	tx.commit();

        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerToggle = new ActionBarDrawerToggle(this, /* host Activity */
        mDrawerLayout, /* DrawerLayout object */
        R.drawable.ic_drawer, /* nav drawer icon to replace 'Up' caret */
        R.string.drawer_open, /* "open drawer" description */
        R.string.drawer_close /* "close drawer" description */
        ) {

            /** Called when a drawer has settled in a completely closed state. */
            @Override
            public void onDrawerClosed(View view)
            {
                getSupportActionBar().setTitle(R.string.main_title);
            }

            /** Called when a drawer has settled in a completely open state. */
            @Override
            public void onDrawerOpened(View drawerView)
            {
                getSupportActionBar().setTitle(R.string.drawer_title);
            }
        };

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setHomeButtonEnabled(true);

        // Place Shadow for NavigationDrawer
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow,
                GravityCompat.START);

        // Fill the menu
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(getSupportActionBar().getThemedContext(),
        		 												android.R.layout.simple_list_item_1,
        		                                                getResources().getStringArray(R.array.main_menu));
        final ListView navList = (ListView) findViewById(R.id.left_drawer);
        navList.setAdapter(adapter);
        navList.setOnItemClickListener(new OnItemClickListener() {
        	@Override
        	public void onItemClick(AdapterView<?> parent, View view, final int pos, long id){
            	FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
            	tx.replace(R.id.main_container, Fragment.instantiate(MainActivity.this, fragments[pos]));
            	tx.commit();
        		mDrawerLayout.closeDrawer(navList);
        	}
		});
        //EntityCreate.CreateEntity();
		new AttemptEntities().execute();        
	}

    @Override
    protected void onPostCreate(Bundle savedInstanceState)
    {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        // Pass the event to ActionBarDrawerToggle, if it returns
        // true, then it has handled the app icon touch event
        if (mDrawerToggle.onOptionsItemSelected(item))
        {
            return true;
        }
        // Handle your other action bar items...

        return super.onOptionsItemSelected(item);
    }
    /*
     * @Override public boolean onCreateOptionsMenu(Menu menu) { // Inflate the
     * menu; this adds items to the action bar if it is present.
     * getMenuInflater().inflate(R.menu.main, menu); return true; }
     */
    class AttemptEntities extends AsyncTask<String, String, String> {

		boolean failure = false;

       @Override
       protected void onPreExecute() {
           super.onPreExecute();
          /* pDialog = new ProgressDialog(EntityManager.this);
           pDialog.setMessage("Chargement ...");
           pDialog.setIndeterminate(false);
           pDialog.setCancelable(true);
           pDialog.show();*/
       }

		@Override
		protected String doInBackground(String... args) {
			// TODO Auto-generated method stub
			
			List<String> tabAnecdote = new LinkedList<String>();
			List<String> tabQuestion = new LinkedList<String>();
			List<Calendar> tabTimeTable = new LinkedList<Calendar>();
			
			List<Menu> tabMenu = new LinkedList<Menu>();
			List<Product> tabProduct = new LinkedList<Product>(); 
			
			// type models
			String[] ArrayAnecdote = new String[0];
			String[] ArrayQuestion = new String[0];
			Menu[] ArrayMenu = new Menu[0];
			Product[] ArrayProduct = new Product[0]; 
			Calendar[] ArrayTimeTable = new Calendar[0];    
			
			SimpleDateFormat CldrShow = new SimpleDateFormat("yyyy-M-dd hh:mm:ss",Locale.FRANCE);
			Date date = new Date();
			Calendar dureeCal = new GregorianCalendar(2013,0,31);
			Calendar creationCal = new GregorianCalendar(2013,0,31);
			    
		    JSONParser jsonParser = new JSONParser(); 
		    String URLEntity = getString(R.string.ENTITY_URL);			
			
			float latitude;
			float longitude;
			List<Show> shows = new LinkedList<Show>();
			List<Establishments> establishments = new LinkedList<Establishments>();			
			
           // Param�tre pour JSON
			List<NameValuePair> params = new ArrayList<NameValuePair>();

		   Log.d("request ", "start");

		   JSONObject json = jsonParser.makeHttpRequest(URLEntity, "GET", params);

		   // check les log de la r�ponse
		   Log.d("Request attempt", json.toString());
		   
			   try {
	
				JSONObject jsonShows = 	json.getJSONObject(getString(R.string.TAG_SHOWS));			   
				   
				//************SHOW**********
				for(Integer i = 0; i < jsonShows.getInt(getString(R.string.TAG_SHOWNUMBER)); i++){
					// Remettre les conteneurs � z�ro
					tabAnecdote = new LinkedList<String>();
					tabQuestion = new LinkedList<String>();
					tabTimeTable = new LinkedList<Calendar>();
					
					   Log.d("Request Successful", "1");	
	
					JSONObject jsonShow = jsonShows.getJSONObject(i.toString());   
					JSONObject jsonAnec = jsonShow.getJSONObject(getString(R.string.TAG_ANECDOTE));	
					
	    			for(Integer j = 0; j < jsonAnec.getInt(getString(R.string.TAG_ANECDOTENUMBER)); j++){
	    				tabAnecdote.add(jsonAnec.getJSONObject(j.toString()).getString(getString(R.string.TAG_INFORMATION)));
	    			}
	    			
					   Log.d("Request Successful", "3");	
					   
					JSONObject jsonQuest = jsonShow.getJSONObject(getString(R.string.TAG_QUESTION));
					
	    			for(Integer j = 0; j < jsonQuest.getInt(getString(R.string.TAG_QUESTIONNUMBER)); j++){
	    				tabQuestion.add(jsonQuest.getJSONObject(j.toString()).getString(getString(R.string.TAG_TEXT)));
	    			} 
	    			
					   Log.d("Request Successful", "4");	
	
					JSONObject jsonTimes = jsonShow.getJSONObject(getString(R.string.TAG_TIMETABLE));
					
	    			for(Integer j = 0; j < jsonTimes.getInt(getString(R.string.TAG_TIMETABLENUMBER)); j++){
						JSONObject jsonTime = jsonTimes.getJSONObject(j.toString()); 	    				
	    				Calendar calendar = Calendar.getInstance(Locale.FRENCH);
	    				date = CldrShow.parse(jsonTime.getString(getString(R.string.TAG_Date)) + " " + jsonTime.getString(getString(R.string.TAG_Hour)));
	    				calendar.setTime(date);
	    				
	    				
	    				tabTimeTable.add(calendar);
	    			}      			
	    			
					   Log.d("Request Successful", "5");	
	    	        latitude = Float.parseFloat(jsonShow.getString(getString(R.string.TAG_LATITUDE)));
	    	        longitude = Float.parseFloat(jsonShow.getString(getString(R.string.TAG_LONGITUDE)));
	    			Coordinates coord = new Coordinates(latitude, longitude);
		
					date = CldrShow.parse(jsonShow.getString(getString(R.string.TAG_CREATIONDATE)) + " " +
							              jsonShow.getString(getString(R.string.TAG_DURATION)));	
					creationCal.setTime(date);									
					dureeCal.setTime(date);
	 			
					shows.add(new Show(jsonShow.getInt(getString(R.string.TAG_NUMBERACTOR)),
							  tabAnecdote.toArray(ArrayAnecdote),
							  coord, creationCal, dureeCal,
							  jsonShow.getString(getString(R.string.TAG_HISTORICALEVENT)),
							  jsonShow.getInt(getString(R.string.TAG_IDSHOW)),
							  jsonShow.getString(getString(R.string.TAG_INFORMATION)),
							  jsonShow.getString(getString(R.string.TAG_LOCATION)),
							  jsonShow.getString(getString(R.string.TAG_NAME)),
							  jsonShow.getInt(getString(R.string.TAG_NOTE)),
							  tabQuestion.toArray(ArrayQuestion),
							  tabTimeTable.toArray(ArrayTimeTable)));
					
				}
				   Log.d("Request Successful", "7");	
				
				//************ESTABLISHMENT**********
				JSONObject jsonEstas = 	json.getJSONObject(getString(R.string.TAG_ESTABLISHMENTS));		
				
				for(Integer i = 0; i < jsonEstas.getInt(getString(R.string.TAG_ESTABLISHMENTSNUMBER)); i++){
					tabMenu = new LinkedList<Menu>();
					tabProduct = new LinkedList<Product>(); 
					JSONObject jsonEsta = jsonEstas.getJSONObject(i.toString()); 					
					JSONObject jsonMenus = 	jsonEsta.getJSONObject(getString(R.string.TAG_MENU));
					
	    			for(Integer j = 0; j < Integer.parseInt(jsonMenus.getString(getString(R.string.TAG_MENUNUMBER))); j++){
						JSONObject jsonMenu = jsonMenus.getJSONObject(j.toString()); 		    				
	    				tabMenu.add(new Menu(jsonMenu.getInt(getString(R.string.TAG_IDMENU)),
	    						    jsonMenu.getString(getString(R.string.TAG_CONTENT)),
	    						    Float.parseFloat(jsonMenu.getString(getString(R.string.TAG_PRICE)))));   				
	    			}
					JSONObject jsonProducts = jsonEsta.getJSONObject(getString(R.string.TAG_PRODUCT));	
					
	    			for(Integer j = 0; j < jsonProducts.getInt(getString(R.string.TAG_PRODUCTNUMBER)); j++){
						JSONObject jsonProduct = jsonProducts.getJSONObject(j.toString()); 		    				
	    				tabProduct.add(new Product(jsonProduct.getInt(getString(R.string.TAG_IDPRODUCT)),
	    						       jsonProduct.getString(getString(R.string.TAG_NAME)),
	    						       Float.parseFloat(jsonProduct.getString(getString(R.string.TAG_PRICE)))));   				
	    			}   			
	    	        
	    	        latitude = Float.parseFloat(jsonEsta.getString(getString(R.string.TAG_LATITUDE)));
	    	        longitude = Float.parseFloat(jsonEsta.getString(getString(R.string.TAG_LONGITUDE)));
	    			Coordinates coord = new Coordinates(latitude, longitude);
	
	    			establishments.add(new Establishments(coord, jsonEsta.getInt(getString(R.string.TAG_IDESTABLISHMENTS)),
	    					                              jsonEsta.getString(getString(R.string.TAG_INFORMATION)),
	    					                              jsonEsta.getString(getString(R.string.TAG_LOCATION)),
	    					                              tabMenu.toArray(ArrayMenu),
	    					                              jsonEsta.getString(getString(R.string.TAG_NAME)),
	    					                              jsonEsta.getInt(getString(R.string.TAG_NOTE)),
	    					                              tabProduct.toArray(ArrayProduct)));
					
				}			
				
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			} catch (java.text.ParseException e1) {
				// TODO Auto-generated catch block
				e1.printStackTrace();
			}
			   ScheduleShow.getInstance().addShows(shows);
			   SingleEstablishments.getInstance().addEstablishments(establishments);		   
	           return null;

			}
			
	       protected void onPostExecute(String file_url) {
	           //pDialog.dismiss();
	       }

	}
    @Override
    public void onBackPressed() {
    	FragmentTransaction tx = getSupportFragmentManager().beginTransaction();
    	tx.replace(R.id.main_container, Fragment.instantiate(MainActivity.this, fragments[0]));
    	tx.commit();
    }
}
