package com.fishroom.view;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.SimpleExpandableListAdapter;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;
import android.widget.TwoLineListItem;

import com.fishroom.entity.Show;
import com.fishroom.falcondroid.R;
import com.fishroom.logic.ScheduleShow;

public class FragmentQueue extends Fragment
{
    private ExpandableListAdapter expList_show;
    private TextView              txt_info_pdf;
    private Button                btn_info_pdf;
    private final String[]        tab_anecdotes = new String[1000];
    private String[]              tab_verif_anecdote;
    private String                anecdote;
    private final Random          nb_rand       = new Random();

    public static Fragment newInstance(Context context)
    {
        FragmentQueue q = new FragmentQueue();

        return q;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        ViewGroup root = (ViewGroup) inflater.inflate(R.layout.fragment_queue,
                null);
        // TextView txt_question = (TextView)
        // root.findViewById(R.id.txt_question);
        TabHost tabhost = (TabHost) root.findViewById(R.id.tabhost);
        tabhost.setup();
        setNewTab(getActivity(), tabhost, "tab1", R.string.queue_tab1,
                R.id.tab1);
        setNewTab(getActivity(), tabhost, "tab2", R.string.queue_tab2,
                R.id.tab2);

        txt_info_pdf = (TextView) root.findViewById(R.id.txt_info_pdf);
        btn_info_pdf = (Button) root.findViewById(R.id.btn_info_pdf);

        Show[] shows = ScheduleShow.getInstance().getShows();
        int test_anecdote = 0;
        tab_anecdotes[0] = "";
        for (int i = 0; i < shows.length; i++)
        {
            Show show = shows[i];
            tab_verif_anecdote = show.getAnecdote();
            if (tab_verif_anecdote.length != 0)
            {
                for (int j = 0; j < tab_verif_anecdote.length; j++)
                {
                    // String st_an = tab_anecdotes[j].toString();
                    if (tab_anecdotes[0] != "")
                    {
                        for (int k = 0; k < tab_anecdotes.length; k++)
                        {
                            if (tab_verif_anecdote[j] == tab_anecdotes[k])
                            {
                                test_anecdote++;
                            }
                        }
                        if (test_anecdote == 0)
                        {
                            for (int l = 0; l < tab_anecdotes.length; l++)
                            {
                                if (tab_anecdotes[l] == null)
                                {
                                    tab_anecdotes[l] = tab_verif_anecdote[j];
                                    break;
                                }
                            }
                        }

                    } else
                    {
                        tab_anecdotes[j] = tab_verif_anecdote[j];
                    }
                }
            }
        }
        int taille_reel_tab_an = 0;
        for (int i = 0; i < tab_anecdotes.length; i++)
        {
            if (tab_anecdotes[i] != null)
            {
                taille_reel_tab_an++;
            }
        }
        final String[] tab_anecdotes_final = new String[taille_reel_tab_an];

        for (int i = 0; i < tab_anecdotes_final.length; i++)
        {
            if (tab_anecdotes[i] != null)
            {
                tab_anecdotes_final[i] = tab_anecdotes[i];
            }
        }

        int an_nb1 = nb_rand.nextInt(tab_anecdotes_final.length);
        txt_info_pdf.setText(tab_anecdotes_final[an_nb1]);

        btn_info_pdf.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v)
            {
                Random nb_rand_clic = new Random();
                int an_nb2 = nb_rand_clic.nextInt(tab_anecdotes_final.length);

                txt_info_pdf.setText(tab_anecdotes_final[an_nb2]);
            }
        });

        return root;
    }

    private static final String NAME    = "NAME";
    private static final String IS_EVEN = "IS_EVEN";

    // private static final String show_id_arg = "showId";

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        List<Map<String, String>> groupData = new ArrayList<Map<String, String>>();
        List<List<Map<String, String>>> childData = new ArrayList<List<Map<String, String>>>();

        Show[] shows = ScheduleShow.getInstance().getShows();
        int test;
        for (int i = 0; i < shows.length; i++)
        {

            Map<String, String> curGroupMap = new HashMap<String, String>();
            groupData.add(curGroupMap);
            curGroupMap.put(NAME, shows[i].getName());
            // curGroupMap.put(IS_EVEN, (i % 2 == 0) ? "This group is even" :
            // "This group is odd");

            // Show show_test = shows[i];
            String[] question = shows[i].getQuestions();

            List<Map<String, String>> children = new ArrayList<Map<String, String>>();

            test = question.length;

            Map<String, String> curChildMap = new HashMap<String, String>();
            children.add(curChildMap);
            curChildMap.put(NAME, "    - " + question[test - 1]);

            /*
             * for (int j = 0; j < question.length; j++) { Map<String, String>
             * curChildMap = new HashMap<String, String>();
             * children.add(curChildMap); curChildMap.put(NAME, question[j]);
             * //curChildMap.put(IS_EVEN, (j % 2 == 0) ? "This child is even" :
             * "This child is odd"); }
             */
            childData.add(children);

        }
        ExpandableListView lv = (ExpandableListView) getActivity()
                .findViewById(R.id.list_info_show);
        // Set up our adapter
        expList_show = new SimpleExpandableListAdapter(getActivity(),
                groupData, android.R.layout.simple_expandable_list_item_2,
                new String[] { NAME, IS_EVEN }, new int[] { android.R.id.text1,
                        android.R.id.text2 }, childData,
                android.R.layout.simple_expandable_list_item_2, new String[] {
                        NAME, IS_EVEN }, new int[] { android.R.id.text1,
                        android.R.id.text2 }) {
            @Override
            public View getChildView(int groupPosition, int childPosition,
                    boolean isLastChild, View convertView, ViewGroup parent)
            {
                TwoLineListItem tv = (TwoLineListItem) super.getChildView(
                        groupPosition, childPosition, isLastChild, convertView,
                        parent);
                final TextView tv1 = (TextView) tv
                        .findViewById(android.R.id.text1);
                final TextView tv2 = (TextView) tv
                        .findViewById(android.R.id.text2);
                tv1.setTextSize(15);
                tv2.setTextSize(15);

                return tv;
            }

        };
        lv.setAdapter(expList_show);
    }

    private void setNewTab(Context context, TabHost tabHost, String tag,
            int title, int contentID)
    {
        TabSpec tabSpec = tabHost.newTabSpec(tag);
        tabSpec.setIndicator(getString(title));
        tabSpec.setContent(contentID);
        tabHost.addTab(tabSpec);
    }
}
