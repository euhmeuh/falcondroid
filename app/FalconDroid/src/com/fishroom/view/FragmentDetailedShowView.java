/**
 * 
 */
package com.fishroom.view;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import org.json.JSONException;
import org.json.JSONObject;

import android.annotation.SuppressLint;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.fishroom.JSON.JSONParser;
import com.fishroom.entity.Show;
import com.fishroom.falcondroid.R;
import com.fishroom.logic.ScheduleShow;

/**
 * @author Antoine-Ali
 * 
 */
public class FragmentDetailedShowView extends Fragment
{

    Show                       show;
    public static final String SHOW_ID_ARG = "showId";

    /**
     * 
     */
    public FragmentDetailedShowView()
    {
        // Vide normal
    }

    @SuppressLint("SimpleDateFormat")
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(
                R.layout.fragment_detailed_show_information, container, false);
        int i = getArguments().getInt(FragmentDetailedShowView.SHOW_ID_ARG);
        show = ScheduleShow.getInstance().getShow(i);

        // Vue
        ImageView image = (ImageView) rootView.findViewById(R.id.ShowImageView);
        int ImageResId = getResources().getIdentifier("show" + show.getId(),
                "drawable", getActivity().getPackageName());
        image.setImageResource(ImageResId);

        // Titre
        TextView text = (TextView) rootView.findViewById(R.id.ShowTitle);
        text.setText(show.getName());
        // Dur�e
        text = (TextView) rootView.findViewById(R.id.ShowDuration);
        Calendar cal = show.getDuration();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(cal.get(Calendar.HOUR));
        stringBuilder.append("h");
        stringBuilder.append(cal.get(Calendar.MINUTE));
        stringBuilder.append("min");
        text.setText(stringBuilder.toString());
        // TODO : Date de Cr�ation
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        text = (TextView) rootView.findViewById(R.id.ShowCreationDate);
        text.setText(getString(R.string.CreationDateTitle) + " "
                + sdf.format(show.getCreationDate().getTime()));
        
        // TimeTable
        text = (TextView) rootView.findViewById(R.id.ShowTimeTable);
        text.setText(show.getMultiLineReadableTimeTable());
      
        // Nombre d'acteurs
        text = (TextView) rootView.findViewById(R.id.ShowActorNumber);
        text.setText(String.valueOf(show.getActorNumber()));
        
        // Description
        text = (TextView) rootView.findViewById(R.id.ShowDesc);
        text.setText(show.getInformation());
        
        // Barre de notation
        RatingBar rate = (RatingBar) rootView.findViewById(R.id.Rating);
        rate.setMax(500);
        rate.setRating(Float.parseFloat(String.valueOf(show.getNote())) / 100.0f);
        rate.setOnRatingBarChangeListener(new changeRatingListener());
    
        // Localisation sur la carte
        text = (TextView) rootView.findViewById(R.id.ShowLocalisationDetails);
        text.setText(show.getLocation());
        
        return rootView;
    }

    private class changeRatingListener implements
            RatingBar.OnRatingBarChangeListener
    {

        /*
         * (non-Javadoc)
         * 
         * @see
         * android.widget.RatingBar.OnRatingBarChangeListener#onRatingChanged
         * (android.widget.RatingBar, float, boolean)
         */
        @Override
        public void onRatingChanged(RatingBar ratingBar, float rating,
                boolean fromUser)
        {
            if (fromUser)
            {
                Float newRating = ((Float.parseFloat(String.valueOf(show
                        .getNote())) * 100.0f) + (rating) * 100.0f) / 2;
                UpdateNotation update = new UpdateNotation();
                update.execute(newRating);
                ratingBar.setRating(newRating / 100.0f);
                ratingBar.setIsIndicator(true);
            }
        }
    }

    public class UpdateNotation extends AsyncTask<Float, Void, Void>
    {
        private final String url = "http://aryl.dnsalias.org/submit.php?type=show";

        @Override
        protected Void doInBackground(Float... params)
        {
            String builded = url + "&id=" + show.getId() + "&note=" + params[0];
            JSONParser parser = new JSONParser();
            JSONObject object = parser.getJSONFromUrl(builded);
            String result;
            try
            {
                result = object.getString("RESULT");
                if (result.equals("true"))
                {
                    Log.i(UpdateNotation.class.toString(), "Notation Saved");
                } else
                {
                    Log.w(UpdateNotation.class.toString(),
                            "Couldn't record notation");
                }
            } catch (JSONException e)
            {
                // TODO Auto-generated catch block
                Log.e(JSONObject.class.toString(), "Failed to parse JSON");
            }
            return null;
        }
    }

}
