package com.fishroom.view;

import java.util.Calendar;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map.Entry;

import android.content.Context;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TabHost;
import android.widget.TabHost.TabSpec;
import android.widget.TextView;

import com.fishroom.adapters.PersonnalScheduleCreationAdapter;
import com.fishroom.adapters.PersonnalScheduleDisplayAdapter;
import com.fishroom.adapters.ShowTimeTableAdapter;
import com.fishroom.entity.Coordinates;
import com.fishroom.entity.Show;
import com.fishroom.falcondroid.R;
import com.fishroom.logic.BestSchedule;
import com.fishroom.logic.Pair;
import com.fishroom.logic.PersonnalSchedule;
import com.fishroom.logic.ScheduleShow;

public class FragmentSchedule extends Fragment implements
        TabHost.OnTabChangeListener
{
    private ListView list;

    public static Fragment newInstance(Context context)
    {
        FragmentSchedule f = new FragmentSchedule();

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        ViewGroup root = (ViewGroup) inflater.inflate(
                R.layout.fragment_schedule, null);

        /* configure tabs */
        TabHost tabhost = (TabHost) root.findViewById(R.id.tabhost);
        tabhost.setup();
        setNewTab(getActivity(), tabhost, "tab1", R.string.schedule_tab1,
                R.id.tab1);
        setNewTab(getActivity(), tabhost, "tab2", R.string.schedule_tab2,
                R.id.tab2);
        setNewTab(getActivity(), tabhost, "tab3", R.string.schedule_tab3,
                R.id.tab3);

        /* load planning content */
        ShowTimeTableAdapter adapter = new ShowTimeTableAdapter(getActivity(),
                ScheduleShow.getInstance().getShows());
        ListView planning = (ListView) root
                .findViewById(R.id.planning_listView);
        planning.setAdapter(adapter);

        /* connect tab signal */
        tabhost.setOnTabChangedListener(this);

        return root;
    }

    private void setNewTab(Context context, TabHost tabHost, String tag,
            int title, int contentID)
    {
        TabSpec tabSpec = tabHost.newTabSpec(tag);
        tabSpec.setIndicator(getString(title));
        tabSpec.setContent(contentID);
        tabHost.addTab(tabSpec);
    }

    @Override
    public void onTabChanged(String tabId)
    {
        /* best schedule */
        if (tabId.equals("tab3"))
        {
            /* get coordinates */
            LocationManager lm = (LocationManager) getActivity()
                    .getApplicationContext().getSystemService(
                            Context.LOCATION_SERVICE);

            // Define a listener that responds to location updates
            LocationListener locationListener = new LocationListener() {
                @Override
                public void onLocationChanged(Location location)
                {

                    /* TODO */
                }

                @Override
                public void onStatusChanged(String provider, int status,
                        Bundle extras)
                {
                }

                @Override
                public void onProviderEnabled(String provider)
                {
                }

                @Override
                public void onProviderDisabled(String provider)
                {
                }
            };

            // Register the listener with the Location Manager to receive
            // location updates
            lm.requestLocationUpdates(LocationManager.GPS_PROVIDER, 0, 0,
                    locationListener);

            // TODO cheat
            Coordinates coor = new Coordinates(46.891966, -0.932498);

            /* load best schedule */
            BestSchedule bs = new BestSchedule(coor, Calendar.getInstance());
            HashMap<Show, Calendar> bestList = bs.getList();

            /* display result */
            for (Show s : bestList.keySet())
            {
                Log.i("Best schedule debug", s.getName() + " "
                        + bestList.get(s).get(Calendar.HOUR_OF_DAY) + "h"
                        + bestList.get(s).get(Calendar.MINUTE));
            }

            /* convert hashmap */
            LinkedList<Pair<Show, Calendar>> list = new LinkedList<Pair<Show, Calendar>>();
            for (Entry<Show, Calendar> e : bestList.entrySet())
            {
                list.add(new Pair<Show, Calendar>(e.getKey(), e.getValue()));
            }

            /* set adapter */
            PersonnalScheduleDisplayAdapter adapter = new PersonnalScheduleDisplayAdapter(
                    getActivity(), list);
            ListView lv = (ListView) getView().findViewById(
                    R.id.best_schedule_listView);
            lv.setAdapter(adapter);

            /* display list view */
            lv.setVisibility(ListView.VISIBLE);

            /* hide spinner */
            ProgressBar sp = (ProgressBar) getView().findViewById(
                    R.id.progressBar1);
            sp.setVisibility(ListView.INVISIBLE);
            TextView tx = (TextView) getView().findViewById(R.id.progressText1);
            tx.setVisibility(ListView.INVISIBLE);

        } else if (tabId.equals("tab2"))
        {
            list = (ListView) getView()
                    .findViewById(R.id.PersonnalScheduleList);

            Button button = (Button) getView().findViewById(R.id.SwitchButton);
            if (PersonnalSchedule.getInstance().defined)
            {
                PersonnalScheduleDisplayAdapter adapter = new PersonnalScheduleDisplayAdapter(
                        getActivity(), PersonnalSchedule.getInstance()
                                .getUserSchedule());
                list.setAdapter(adapter);
                button.setText(R.string.ChangePlanning);
                button.setOnClickListener(new SwitchClickListener());
            } else
            {
                PersonnalScheduleCreationAdapter adapter = new PersonnalScheduleCreationAdapter(
                        getActivity());
                list.setAdapter(adapter);
                button.setText(R.string.DefinePlanning);
                button.setOnClickListener(new SwitchClickListener());
            }
        }
    }

    private class SwitchClickListener implements OnClickListener
    {

        @Override
        public void onClick(View v)
        {
            if (PersonnalSchedule.getInstance().defined)
            {
                PersonnalSchedule.getInstance().defined = false;
                Button button = (Button) v;
                button.setText(R.string.DefinePlanning);
                PersonnalScheduleCreationAdapter adapter = new PersonnalScheduleCreationAdapter(
                        getActivity());
                list.setAdapter(adapter);

            } else
            {
                PersonnalSchedule.getInstance().defined = true;
                Button button = (Button) v;
                button.setText(R.string.ChangePlanning);
                PersonnalScheduleDisplayAdapter adapter = new PersonnalScheduleDisplayAdapter(
                        getActivity(), PersonnalSchedule.getInstance()
                                .getUserSchedule());
                list.setAdapter(adapter);
            }

        }

    }
}
