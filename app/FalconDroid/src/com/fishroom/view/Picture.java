package com.fishroom.view;

import java.io.FileOutputStream;

import com.fishroom.falcondroid.R;

import android.R.color;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TabHost;


public class Picture extends Fragment implements TabHost.OnTabChangeListener
{

    private static final int CAMERA_REQUEST = 1888; 
    private ImageView imageView;
    private Button button;
    private Bitmap photoModif;
    private static Bitmap testFiltre;
	
    public static Fragment newInstance(Context context)
    {
        FragmentSchedule f = new FragmentSchedule();

        return f;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState)
    {
        ViewGroup root = (ViewGroup) inflater.inflate(
                R.layout.picture, null);
        
        this.imageView = (ImageView)root.findViewById(R.id.imageView1);
        this.button = (Button)root.findViewById(R.id.savePicture);
        
        button.setOnClickListener(btnSavePictureListener);        
        
        dispatchTakePictureIntent(CAMERA_REQUEST);      
        
		return root;
    }
    
    
    private void dispatchTakePictureIntent(int actionCode) {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        startActivityForResult(takePictureIntent, actionCode);
    }    

    public void onActivityResult(int requestCode, int resultCode, Intent data) {  
 
    		Point Position = new Point(20,120);
    		Color Couleur = new Color();
    		
    		Drawable iconDrawable =  getActivity().getApplicationContext().getResources().getDrawable(R.drawable.gladiator);
    		
    		testFiltre = Bitmap.createBitmap(100, 100, Bitmap.Config.ARGB_4444);  // A v�rifier si c'est la bonne config � utiliser
    		Canvas canvas = new Canvas(testFiltre); 
    		iconDrawable.setBounds(0, 0, 100, 100); 
    		iconDrawable.draw(canvas);
    		
            Bitmap photo = (Bitmap) data.getExtras().get("data");
            photoModif = mark(photo,"Photo prise au Puy Du Fou !!!", Position, Couleur.BLACK, 250, 20, true);
            imageView.setImageBitmap(photoModif); 
    }    
    
    public static Bitmap mark(Bitmap src, String watermark, Point location, int color, int alpha, int size, boolean underline) {
    	int w = src.getWidth();
    	int h = src.getHeight();
    	Bitmap result = Bitmap.createBitmap(w, h, src.getConfig());

    	Canvas canvas = new Canvas(result);
    	canvas.drawBitmap(src, 0, 0, null);

    	Paint paint = new Paint();
    	paint.setColor(color);
    	paint.setAlpha(alpha);
    	paint.setTextSize(size);
    	paint.setAntiAlias(true);
    	paint.setUnderlineText(underline);
    	//canvas.drawText(watermark, location.x, location.y, paint);
    	//Fonction pour ajouter image :
    	canvas.drawBitmap(testFiltre, location.x, location.y, paint);

    	return result;
    	}   
    
	private OnClickListener btnSavePictureListener = new OnClickListener() {
		
		@Override
		public void onClick(View actuelView) {
			// TODO Auto-generated method stub
			try {
				   FileOutputStream fos=new FileOutputStream("/mnt/sdcard/Pictures/photo.png");
				   photoModif.compress(CompressFormat.PNG, 100, fos); 
		
				   fos.flush();
				   fos.close();
				   } catch (Exception e) {
				   Log.e("MyLog", e.toString());
				}			
		}
	};    
    
	@Override
	public void onTabChanged(String arg0) {
		// TODO Auto-generated method stub
		
	}
}    
