package com.fishroom.logic;

public class Triplet<X, Y, Z>
{
    public X left;
    public Y center;
    public Z right;

    public Triplet(X left, Y center, Z right)
    {
        this.left = left;
        this.right = right;
        this.center = center;
    }
}
