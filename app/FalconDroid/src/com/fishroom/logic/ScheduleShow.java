package com.fishroom.logic;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import android.util.Log;

import com.fishroom.entity.Show;

public class ScheduleShow
{

    private int nb_shows;
    private final HashMap<Integer, Show> shows;

    private ScheduleShow()
    {
        nb_shows = 0;
        shows = new HashMap<Integer, Show>();
    }

    private static class SingletonHolder
    {
        private final static ScheduleShow instance = new ScheduleShow();
    }

    public static ScheduleShow getInstance()
    {
        return SingletonHolder.instance;
    }

    public void addShows(List<Show> shows)
    {

        nb_shows = shows.size();
        for (Show s : shows)
        {
            this.shows.put(s.getId(), s);
        }
    }

    public Show[] getShows()
    {
        Show[] showArray = new Show[0];
        return shows.values().toArray(showArray);
    }


    public Show getShow(int id)
    {
        return this.shows.get(id);
    }

    public HashMap<Show, Calendar> getShowsAtDate(Calendar date, int offset) {
    	HashMap<Show, Calendar> result = new HashMap<Show, Calendar>();
    	
    	/* conversion */
    	int date_time = date.get(Calendar.HOUR_OF_DAY)*3600 + date.get(Calendar.MINUTE)*60;
    	
    	for(Show s : shows.values()) {
    		for(Calendar c : s.getTimeTable()) {
    			/* convert date into seconds */
    			int show_time = c.get(Calendar.HOUR_OF_DAY)*3600 + c.get(Calendar.MINUTE)*60;
    			
    			/* check if the time is between date and date+offset  */
    			if(show_time >= date_time && show_time <= date_time+offset) {
    				result.put(s, c);
    				break;
    			}
    			
    		}
    	}
    	
    	return result;
    }
    
}
