package com.fishroom.logic;

public class Pair<X, Y> { 
	public final X left; 
	public final Y right; 
	
	public Pair() {
		this.left = null;
		this.right = null;
	}
	
	public Pair(X left, Y right) { 
		this.left = left; 
		this.right = right; 
	}
}
