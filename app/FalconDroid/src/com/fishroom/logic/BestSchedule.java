package com.fishroom.logic;

import java.util.Calendar;
import java.util.HashMap;
import java.util.Random;

import android.location.Location;
import android.util.Log;

import com.fishroom.entity.Coordinates;
import com.fishroom.entity.Show;

public class BestSchedule {
	
	private HashMap<Show, Calendar> list;
	
	public HashMap<Show, Calendar> getList() {
		return list;
	}

	public void setList(HashMap<Show, Calendar> list) {
		this.list = list;
	}

	public BestSchedule(Coordinates pos, Calendar date) {
		
		/* this list will contain the best schedule (final result) */
		list = new HashMap<Show, Calendar>();
		
		/* get the first event/time pair */
		/* left : show infos | right : chosen time */
		Pair<Show, Calendar> e = findBestEvent(pos, date);
		
		while(e != null) {
			/* add to the list */
			list.put(e.left, e.right);
			
			/* add duration to the event date */
			Calendar duration = e.left.getDuration();
			Calendar new_date = (Calendar) e.right.clone();
			new_date.add(Calendar.HOUR_OF_DAY, duration.get(Calendar.HOUR_OF_DAY));
			new_date.add(Calendar.MINUTE, duration.get(Calendar.MINUTE));
			
			/* try to find the next event */
			e = findBestEvent(e.left.getCoordinates(), new_date);
		}
	}
	
	public Pair<Show, Calendar> findBestEvent(Coordinates pos, Calendar date) {
		/* get upcoming events */
		ScheduleShow schedule = ScheduleShow.getInstance();
		HashMap<Show, Calendar> events = schedule.getShowsAtDate(date, 3600);
		
		/* check distance of each eligible event */
		Pair<Show, Long> eligible = null;
		Long travelTime = (long) 0;
		for(Show e : events.keySet()) {
			/* get the time that will be spent walking to the next event */
			travelTime = getTravelTime(pos, e.getCoordinates());
			
			if(eligible != null) {
				/* if this time is better than the previous one */
				if(travelTime < eligible.right) {
					/* keep this event */
					eligible = new Pair<Show, Long>(e, travelTime);
				} else {
					continue;
				}
			} else {
				eligible = new Pair<Show, Long>(e, travelTime);
			}
		}
		
		/* return the best event */
		if(eligible == null) {
			return null;
		} else {
			return new Pair<Show, Calendar>(eligible.left, events.get(eligible.left));
		}
	}
	
	public long getTravelTime(Coordinates pos1, Coordinates pos2) {
		/* cast into google Location type */
		//Location loc1 = new Location();
		return (new Random()).nextInt(100);
	}
}
