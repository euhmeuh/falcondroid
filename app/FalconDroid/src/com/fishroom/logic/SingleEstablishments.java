package com.fishroom.logic;

import java.util.HashMap;
import java.util.List;

import android.annotation.SuppressLint;

import com.fishroom.entity.Establishments;
import com.fishroom.entity.Menu;
import com.fishroom.entity.Product;

public class SingleEstablishments {
	private int nb_establishments;
	private int nb_menu;
	private int nb_product;
	private final HashMap<Integer, Establishments> establishments;
	private final HashMap<Integer, Menu> menu;
	private final HashMap<Integer, Product> product;

	@SuppressLint("UseSparseArrays")
	private SingleEstablishments() {
		establishments = new HashMap<Integer, Establishments>();
		menu = new HashMap<Integer, Menu>();
		product = new HashMap<Integer, Product>();
	}

	private static class SingletonHolder {
		private final static SingleEstablishments instance = new SingleEstablishments();
	}

	public static SingleEstablishments getInstance() {
		return SingletonHolder.instance;
	}

	/*
	 * ---------------------------------------------------------------------
	 * Establishments
	 * ---------------------------------------------------------------------
	 */
	public void addEstablishments(List<Establishments> establishments) {
		this.setNb_establishments(establishments.size());
		for (Establishments e : establishments) {
			this.establishments.put(e.getId(), e);
		}
	}

	public Establishments[] getEstablishments() {
		Establishments[] establishmentsArray = new Establishments[0];
		return this.establishments.values().toArray(establishmentsArray);
	}

	public Establishments getEstablishments(int id) {
		return this.establishments.get(id);
	}

	/*
	 * ---------------------------------------------------------------------
	 * Menu
	 * ---------------------------------------------------------------------
	 */
	public void addMenu(List<Menu> menu) {
		this.setNb_menu(menu.size());
		for (Menu m : menu) {
			this.menu.put(m.getId(), m);
		}
	}

	public Menu[] getMenu() {
		Menu[] menuArray = new Menu[0];
		return this.menu.values().toArray(menuArray);
	}

	public Menu getMenu(int id) {
		return this.menu.get(id);
	}

	/*
	 * ---------------------------------------------------------------------
	 * Product
	 * ---------------------------------------------------------------------
	 */
	public void addProduct(List<Product> product) {
		this.setNb_product(product.size());
		for (Product p : product) {
			this.product.put(p.getId(), p);
		}
	}

	public Product[] getProduct() {
		Product[] productsArray = new Product[0];
		return this.product.values().toArray(productsArray);
	}

	public Product getProduct(int id) {
		return this.product.get(id);
	}

	/*
	 * ---------------------------------------------------------------------
	 * Getter and Setter
	 * ---------------------------------------------------------------------
	 */
	public int getNb_establishments() {
		return nb_establishments;
	}

	public void setNb_establishments(int nb_establishments) {
		this.nb_establishments = nb_establishments;
	}

	public int getNb_menu() {
		return nb_menu;
	}

	public void setNb_menu(int nb_menu) {
		this.nb_menu = nb_menu;
	}

	public int getNb_product() {
		return nb_product;
	}

	public void setNb_product(int nb_product) {
		this.nb_product = nb_product;
	}
}
