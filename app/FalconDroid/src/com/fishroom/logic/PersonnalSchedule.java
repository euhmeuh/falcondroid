package com.fishroom.logic;

import java.util.Calendar;
import java.util.LinkedList;
import java.util.List;

import com.fishroom.entity.Show;

public class PersonnalSchedule
{
    public LinkedList<Pair<Show, Calendar>>       userSchedule;
    public List<Triplet<Show, Calendar, Boolean>> completeSchedule;
    public Boolean                                defined;

    private static class SingletonHolder
    {
        private final static PersonnalSchedule instance = new PersonnalSchedule();
    }

    private PersonnalSchedule()
    {
        this.defined = false;
        userSchedule = new LinkedList<Pair<Show, Calendar>>();
        completeSchedule = new LinkedList<Triplet<Show, Calendar, Boolean>>();
        for (Show s : ScheduleShow.getInstance().getShows())
        {
            for (Calendar cal : s.getTimeTable())
            {
                completeSchedule.add(new Triplet<Show, Calendar, Boolean>(s,
                        cal, false));
            }
        }
    }

    public LinkedList<Pair<Show, Calendar>> getUserSchedule()
    {
        userSchedule.clear();
        for (Triplet<Show, Calendar, Boolean> t : completeSchedule)
        {
            if (t.right)
            {
                userSchedule.add(new Pair<Show, Calendar>(t.left, t.center));
            }
        }
        this.defined = true;
        return userSchedule;
    }

    public static PersonnalSchedule getInstance()
    {
        return SingletonHolder.instance;
    }
}
