/**
 * 
 */
package com.fishroom.adapters;

import java.util.Calendar;
import java.util.LinkedList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.fishroom.entity.Show;
import com.fishroom.falcondroid.R;
import com.fishroom.logic.Pair;

/**
 * @author Antoine-Ali
 * 
 */
public class PersonnalScheduleDisplayAdapter extends BaseAdapter implements
        ListAdapter
{
    private final Context                          context;
    private final LinkedList<Pair<Show, Calendar>> shows;

    public PersonnalScheduleDisplayAdapter(Context context,
            LinkedList<Pair<Show, Calendar>> shows)
    {

        super();
        this.context = context;
        this.shows = shows;
    }

    @Override
    public Pair<Show, Calendar> getItem(int position)
    {
        return this.shows.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.adapter_show_name_and_time,
                parent, false);

        TextView showName = (TextView) rowView
                .findViewById(R.id.showNameTimeAdapter_Name);
        showName.setText(this.shows.get(position).left.getName());

        TextView showTime = (TextView) rowView
                .findViewById(R.id.showNameTimeAdapter_Time);
        showTime.setText(Show.cradeDateBuilder(this.shows.get(position).right));

        return rowView;
    }

    @Override
    public int getCount()
    {
        return this.shows.size();
    }

    @Override
    public long getItemId(int position)
    {
        return this.shows.get(position).left.getId();
    }
}
