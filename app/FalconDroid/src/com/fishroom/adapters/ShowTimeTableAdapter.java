/**
 * 
 */
package com.fishroom.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.fishroom.falcondroid.R;
import com.fishroom.entity.Show;

/**
 * @author Antoine-Ali
 * 
 */
public class ShowTimeTableAdapter extends ArrayAdapter<Show>
{
    private final Context context;
    private final Show[]  shows;

    public ShowTimeTableAdapter(Context context, Show[] objects)
    {
        super(context, R.layout.adapter_show_name_and_time, objects);
        this.context = context;
        this.shows = objects;
    }

    @Override
    public Show getItem(int position)
    {
        return this.shows[position];
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.adapter_show_name_and_time, parent,
                false);
        
        TextView showName = (TextView) rowView.findViewById(R.id.showNameTimeAdapter_Name);
        showName.setText(this.shows[position].getName());

        TextView showTime = (TextView) rowView.findViewById(R.id.showNameTimeAdapter_Time);
        showTime.setText(this.shows[position].getOneLineReadableTimeTable());
        
        return rowView;
    }

}
