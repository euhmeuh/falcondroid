package com.fishroom.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.fishroom.entity.Establishments;
import com.fishroom.falcondroid.R;

/**
 * @author Julien
 * 
 */
public class ServiceAdapter extends ArrayAdapter<Establishments> {
	private final Context context;
	private final Establishments[] establishments;

	/**
	 * @param context
	 * @param objects
	 */
	public ServiceAdapter(Context context, Establishments[] objects) {
		super(context, R.layout.adapter_service_name, objects);
		this.context = context;
		this.establishments = objects;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ArrayAdapter#getItem(int)
	 */
	@Override
	public Establishments getItem(int position) {
		return this.establishments[position];
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see android.widget.ArrayAdapter#getView(int, android.view.View,
	 * android.view.ViewGroup)
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		LayoutInflater inflater = (LayoutInflater) context
				.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

		View rowView = inflater.inflate(R.layout.adapter_service_name, parent,
				false);

		TextView establishmentsName = (TextView) rowView
				.findViewById(R.id.serviceName);
		establishmentsName.setText(this.establishments[position].getName());

		return rowView;
	}
}
