/**
 * 
 */
package com.fishroom.adapters;

import java.util.Calendar;
import java.util.List;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ListAdapter;
import android.widget.TextView;

import com.fishroom.entity.Show;
import com.fishroom.falcondroid.R;
import com.fishroom.logic.PersonnalSchedule;
import com.fishroom.logic.Triplet;

/**
 * @author Antoine-Ali
 * 
 */
public class PersonnalScheduleCreationAdapter extends BaseAdapter implements
        ListAdapter
{
    private final Context                                context;
    private final List<Triplet<Show, Calendar, Boolean>> shows;
    private Integer                                      curPos;

    public PersonnalScheduleCreationAdapter(Context context)
    {

        super();
        this.context = context;
        this.shows = PersonnalSchedule.getInstance().completeSchedule;
    }

    protected void changeState(int position, Boolean state)
    {
        List<Triplet<Show, Calendar, Boolean>> temp = shows;
        Triplet<Show, Calendar, Boolean> tempTrip = new Triplet<Show, Calendar, Boolean>(
                shows.get(position).left, shows.get(position).center, state);
        temp.set(position, tempTrip);
    }

    @Override
    public Triplet<Show, Calendar, Boolean> getItem(int position)
    {
        return this.shows.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        curPos = position;
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(
                R.layout.adapter_show_name_time_checkbox, parent, false);

        TextView showName = (TextView) rowView
                .findViewById(R.id.showNameTimeCheckBoxAdapter_Name);
        showName.setText(this.shows.get(position).left.getName());

        TextView showTime = (TextView) rowView
                .findViewById(R.id.showNameTimeCheckBoxAdapter_Time);
        showTime.setText(Show.cradeDateBuilder(this.shows.get(position).center));

        CheckBox checkBox = (CheckBox) rowView
                .findViewById(R.id.showNameTimeCheckBoxAdapter_CheckBox);
        checkBox.setChecked(this.shows.get(position).right);
        checkBox.setOnCheckedChangeListener(new ItemSelectionListener(position));
        return rowView;
    }

    @Override
    public int getCount()
    {
        return this.shows.size();
    }

    @Override
    public long getItemId(int position)
    {
        return this.shows.get(position).left.getId();
    }

    private class ItemSelectionListener implements OnCheckedChangeListener
    {
        private final int position;

        public ItemSelectionListener(int position)
        {
            this.position = position;
        }

        @Override
        public void onCheckedChanged(CompoundButton buttonView,
                boolean isChecked)
        {
            changeState(position, isChecked);

        }

    }
}
