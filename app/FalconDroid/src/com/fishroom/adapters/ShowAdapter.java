/**
 * 
 */
package com.fishroom.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.fishroom.entity.Show;
import com.fishroom.falcondroid.R;

/**
 * @author Antoine-Ali
 * 
 */
public class ShowAdapter extends ArrayAdapter<Show>
{
    private final Context context;
    private final Show[]  shows;

    public ShowAdapter(Context context, Show[] objects)
    {
        super(context, R.layout.adapter_show_name, objects);
        this.context = context;
        this.shows = objects;
    }

    @Override
    public Show getItem(int position)
    {
        return this.shows[position];
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent)
    {
        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        View rowView = inflater.inflate(R.layout.adapter_show_name, parent,
                false);

        TextView showName = (TextView) rowView.findViewById(R.id.showName_Name);
        showName.setText(this.shows[position].getName());

        return rowView;
    }

}
