-- phpMyAdmin SQL Dump
-- version 4.0.9
-- http://www.phpmyadmin.net
--
-- Client: 10.144.127.17
-- Généré le: Lun 09 Décembre 2013 à 10:33
-- Version du serveur: 5.5.24
-- Version de PHP: 5.5.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données: `falcondroid`
--
CREATE DATABASE IF NOT EXISTS `falcondroid` DEFAULT CHARACTER SET latin1 COLLATE latin1_swedish_ci;
USE `falcondroid`;

-- --------------------------------------------------------

--
-- Structure de la table `anecdote`
--

DROP TABLE IF EXISTS `anecdote`;
CREATE TABLE IF NOT EXISTS `anecdote` (
  `ID_Anecdote` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Show` int(11) DEFAULT NULL,
  `Information` varchar(800) DEFAULT NULL,
  PRIMARY KEY (`ID_Anecdote`),
  KEY `ID_Show` (`ID_Show`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- RELATIONS POUR LA TABLE `anecdote`:
--   `ID_Show`
--       `show` -> `ID_Show`
--

-- --------------------------------------------------------

--
-- Structure de la table `establishment`
--

DROP TABLE IF EXISTS `establishment`;
CREATE TABLE IF NOT EXISTS `establishment` (
  `ID_Shop` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Note` int(11) DEFAULT NULL,
  `Information` varchar(800) DEFAULT NULL,
  `Localisation` int(11) DEFAULT NULL,
  `Longitude` float DEFAULT NULL,
  `Latitude` float DEFAULT NULL,
  PRIMARY KEY (`ID_Shop`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `menu`
--

DROP TABLE IF EXISTS `menu`;
CREATE TABLE IF NOT EXISTS `menu` (
  `ID_Menu` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Restaurant` int(11) NOT NULL,
  `Price` double NOT NULL,
  `Content` varchar(255) NOT NULL,
  PRIMARY KEY (`ID_Menu`),
  KEY `ID_Restaurant` (`ID_Restaurant`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- RELATIONS POUR LA TABLE `menu`:
--   `ID_Restaurant`
--       `establishment` -> `ID_Shop`
--

-- --------------------------------------------------------

--
-- Structure de la table `product`
--

DROP TABLE IF EXISTS `product`;
CREATE TABLE IF NOT EXISTS `product` (
  `ID_Product` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) NOT NULL,
  `Price` double NOT NULL,
  PRIMARY KEY (`ID_Product`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `question`
--

DROP TABLE IF EXISTS `question`;
CREATE TABLE IF NOT EXISTS `question` (
  `ID_Question` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Show` int(11) NOT NULL,
  `Text` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`ID_Question`),
  KEY `ID_Show` (`ID_Show`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Structure de la table `sell`
--

DROP TABLE IF EXISTS `sell`;
CREATE TABLE IF NOT EXISTS `sell` (
  `ID_Shop` int(11) NOT NULL,
  `ID_Product` int(11) NOT NULL,
  KEY `ID_Shop` (`ID_Shop`,`ID_Product`),
  KEY `ID_Product` (`ID_Product`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- RELATIONS POUR LA TABLE `sell`:
--   `ID_Shop`
--       `establishment` -> `ID_Shop`
--   `ID_Product`
--       `product` -> `ID_Product`
--

-- --------------------------------------------------------

--
-- Structure de la table `show`
--

DROP TABLE IF EXISTS `show`;
CREATE TABLE IF NOT EXISTS `show` (
  `ID_Show` int(11) NOT NULL AUTO_INCREMENT,
  `Name` varchar(255) DEFAULT NULL,
  `Note` int(11) DEFAULT NULL,
  `Information` varchar(800) DEFAULT NULL,
  `Duration` time DEFAULT NULL,
  `Creation_Date` date DEFAULT NULL,
  `Number_Actor` int(11) DEFAULT NULL,
  `Historical_Event` varchar(800) DEFAULT NULL,
  `Localisation` int(11) DEFAULT NULL,
  `Longitude` float DEFAULT NULL,
  `Latitude` float DEFAULT NULL,
  PRIMARY KEY (`ID_Show`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- RELATIONS POUR LA TABLE `show`:
--   `ID_Show`
--       `question` -> `ID_Show`
--

-- --------------------------------------------------------

--
-- Structure de la table `timetable`
--

DROP TABLE IF EXISTS `timetable`;
CREATE TABLE IF NOT EXISTS `timetable` (
  `ID_Timetable` int(11) NOT NULL AUTO_INCREMENT,
  `ID_Show` int(11) NOT NULL,
  `Play_Date` date DEFAULT NULL,
  `Play_Hour` time DEFAULT NULL,
  PRIMARY KEY (`ID_Timetable`),
  KEY `ID_Show` (`ID_Show`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- RELATIONS POUR LA TABLE `timetable`:
--   `ID_Show`
--       `show` -> `ID_Show`
--

--
-- Contraintes pour les tables exportées
--

--
-- Contraintes pour la table `anecdote`
--
ALTER TABLE `anecdote`
  ADD CONSTRAINT `anecdote_ibfk_1` FOREIGN KEY (`ID_Show`) REFERENCES `show` (`ID_Show`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `menu`
--
ALTER TABLE `menu`
  ADD CONSTRAINT `menu_ibfk_1` FOREIGN KEY (`ID_Restaurant`) REFERENCES `establishment` (`ID_Shop`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `sell`
--
ALTER TABLE `sell`
  ADD CONSTRAINT `sell_ibfk_1` FOREIGN KEY (`ID_Shop`) REFERENCES `establishment` (`ID_Shop`) ON DELETE CASCADE ON UPDATE NO ACTION,
  ADD CONSTRAINT `sell_ibfk_2` FOREIGN KEY (`ID_Product`) REFERENCES `product` (`ID_Product`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `show`
--
ALTER TABLE `show`
  ADD CONSTRAINT `show_ibfk_1` FOREIGN KEY (`ID_Show`) REFERENCES `question` (`ID_Show`) ON DELETE CASCADE ON UPDATE NO ACTION;

--
-- Contraintes pour la table `timetable`
--
ALTER TABLE `timetable`
  ADD CONSTRAINT `timetable_ibfk_1` FOREIGN KEY (`ID_Show`) REFERENCES `show` (`ID_Show`) ON DELETE CASCADE ON UPDATE NO ACTION;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
